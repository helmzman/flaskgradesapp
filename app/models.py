from app import db, login
from flask_login import UserMixin


class User(UserMixin, db.Model):
    userid = db.Column(db.Integer, primary_key=True)
    usertype = db.Column(db.String(255), index=True)
    firstname = db.Column(db.String(255), index=True)
    lastname = db.Column(db.String(255), index=True)
    username = db.Column(db.String(255), index=True)
    password = db.Column(db.String(255))
    teachers = db.relationship('Class', backref='teacher', lazy=True)
    students = db.relationship('Grade', backref='student', lazy=True)

    def set_password(self, password):
        # self.password_hash = generate_password_hash(password)
        self.password = password

    def check_password(self, password):
        # return check_password_hash(self.password_hash, password)
        return self.password == password
    
    def get_id(self):
        return self.userid

    def get_grades(self):
        return Grade.query.filter_by(studentid = self.userid)
    
    def get_taught_classes(self):
        return Class.query.filter_by(teacherid = self.userid)

class Class(db.Model):
    classid = db.Column(db.Integer, primary_key=True)
    classname = db.Column(db.String(255), index=True, nullable=False)
    teacherid = db.Column(db.Integer, db.ForeignKey('user.userid'), nullable=False)
    grades = db.relationship('Grade', backref='gclass', lazy=True)

    def get_id(self):
        return self.classid

    def get_all_grades(self):
        return Grade.query.filter_by(classid = self.classid)

class Grade(db.Model):
    gradeid = db.Column(db.Integer, primary_key=True)
    grade = db.Column(db.Integer, nullable=True)
    studentid = db.Column(db.Integer, db.ForeignKey('user.userid'), nullable=False)
    classid = db.Column(db.Integer, db.ForeignKey('class.classid'), nullable=False)

    def get_id(self):
        return self.gradeid

@login.user_loader
def load_user(id):
    return User.query.get(int(id))
