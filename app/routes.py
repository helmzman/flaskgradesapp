from flask import render_template, redirect, url_for, flash, request
from app import app, db
from flask_login import current_user, login_user, login_required, logout_user
from app.forms import LoginForm, RegistrationForm
from app.models import User
import app.models as models
from werkzeug.urls import url_parse

@app.route('/')
@app.route('/index')
@login_required
def index():
	args = {}
	args['available_classes'] = models.Class.query.all()
	if current_user.usertype == 'student':
		args['grades'] = current_user.get_grades()
	return render_template('home.html', title="Home", args=args)

@app.route('/login', methods=['GET', 'POST'])
def login():
	if current_user.is_authenticated:
		return redirect(url_for('index'))
	form = LoginForm()
	if form.validate_on_submit():
		user = User.query.filter_by(username=form.username.data).first()
		if user is None or not user.check_password(form.password.data):
			flash('Invalid username or password')
			return redirect(url_for('index'))
		login_user(user, remember=form.remember_me.data)
		next_page = request.args.get('next')
		if not next_page or url_parse(next_page).netloc != '':
			next_page = url_for('index')
		return redirect(next_page)
	return render_template('login.html', title='Sign In', form=form)

@app.route('/logout')
def logout():
	logout_user()
	return redirect(url_for('index'))

@app.route('/register', methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = RegistrationForm()
    if form.validate_on_submit():
        user = User(username=form.username.data, usertype=form.usertype.data, firstname=form.firstname.data, lastname=form.lastname.data)
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        flash('Congratulations, you are now a registered user!')
        return redirect(url_for('login'))
    return render_template('register.html', title='Register', form=form)

@app.route('/joinclass', methods=['POST'])
@login_required
def joinclass():
	# Teachers shouldn't be able to join classes
	if current_user.usertype == 'teacher':
		return redirect(url_for('index'))

	# Should only join a class if the student is not in a class already and the class exists
	selected_classid = request.form.get('selected_class')
	selected_class = models.Class.query.get(selected_classid)
	if selected_class is None:
		return redirect(url_for('index'))
	res_grades = models.Grade.query.filter(models.Grade.classid == selected_classid, models.Grade.studentid == current_user.userid).all()
	if len(res_grades) > 0:
		print('Already in class')
		return redirect(url_for('index'))

	# To join class, create a new Grade record with null grade linking the student to the class 
	newgrade = models.Grade(studentid=current_user.userid, classid=selected_classid)
	db.session.add(newgrade)
	db.session.commit()

	return redirect(url_for('index'))

@app.route('/leaveclass', methods=['POST'])
@login_required
def leaveclass():
	# Only students should be able to leave a class
	if current_user.usertype == 'teacher':
		return redirect(url_for('index'))
	
	# To leave a class means removing the link between the class and student
	# Should only be able to leave the class if this link exists
	selected_gradeid = request.form.get('gradetoleave')
	selected_grade = models.Grade.query.get(selected_gradeid)
	if selected_grade is not None:
		models.Grade.query.filter_by(gradeid = selected_gradeid).delete()
		db.session.commit()
	return redirect(url_for('index'))

@app.route('/deleteaccount', methods=['POST'])
@login_required
def deleteaccount():
	uid = current_user.userid
	models.User.query.filter_by(userid = uid).delete()
	db.session.commit()
	logout_user()
	return redirect(url_for('index'))

@app.route('/changegrade', methods=['POST'])
@login_required
def changegrade():
	if current_user.usertype == 'student':
		return redirect(url_for('index'))

	gradetochange = request.form.get('gradetochange')
	newgrade = request.form.get('newgrade')

	# Grade must exist, and it must be for a class that the current user is the teacher of 
	grade = models.Grade.query.filter_by(gradeid = gradetochange).first()
	if grade is not None and grade.gclass.teacherid == current_user.userid:
		print('Updating grade')
		grade.grade = int(newgrade)
		db.session.commit()
	
	return redirect(url_for('index'))

@app.route('/deletegrade', methods=['POST'])
@login_required
def deletegrade():
	if current_user.usertype == 'student':
		return redirect(url_for('index'))
	
	gradetochange = request.form.get('gradetochange')
	grade = models.Grade.query.filter_by(gradeid = gradetochange).first()
	if grade is not None:
		models.Grade.query.filter_by(gradeid = gradetochange).delete()
		db.session.commit()

	return redirect(url_for('index'))

@app.route('/createclass', methods=['POST'])
@login_required
def createclass():
	if current_user.usertype == 'student':
		return redirect(url_for('index'))
	
	new_class_name = request.form.get('classname')
	db.session.add(models.Class(teacherid=current_user.userid, classname=new_class_name))
	db.session.commit()

	return redirect(url_for('index'))