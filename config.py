import os
basedir = os.path.abspath(os.path.dirname(__file__))


class Config():
	# SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or \
    #    'sqlite:///' + os.path.join(basedir, 'app.db')
    SQLALCHEMY_DATABASE_URI = f'mysql+pymysql://root:{os.environ.get("DBPASSWORD")}@104.198.35.213/DBSecAssignment'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'you-will-never-guess'
